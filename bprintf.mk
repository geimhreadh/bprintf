# Overridable parameters.
BPRINTF_ROOT       ?= bprintf
BPRINTF_NARGS      ?= 10

BPRINTF_MAJOR       = 0
BPRINTF_MINOR       = 1
BPRINTF_PATCH       = 0

BPRINTF_VERSION     = $(BPRINTF_MAJOR).$(BPRINTF_MINOR).$(BPRINTF_PATCH)
BPRINTF_HEADER      = $(BPRINTF_AUTOGEN_DIR)/bprintf_gen.h
BPRINTF_SECTION     = .bprintf_rodata

DEFINES.bprintf     = BPRINTF_VERSION=$(BPRINTF_VERSION) BPRINTF_HEADER=$(BPRINTF_HEADER) \
					  BPRINTF_SECTION=\"$(BPRINTF_SECTION)\"

INCDIR.bprintf      = $(BPRINTF_ROOT)/inc
SRCDIRS.bprintf    := $(BPRINTF_ROOT)/src
SOURCES_CC.bprintf  = $(foreach sdir,$(SRCDIRS.bprintf),$(wildcard $(sdir)/*.c))
OBJECTS_CC.bprintf  = $(patsubst %.c,$(BUILDROOT)/%.o,$(SOURCES_CC.bprintf))

bprintf_prehook: $(BPRINTF_HEADER)

$(BPRINTF_HEADER):
	@echo "BPRINTF v$(BPRINTF_MAJOR).$(BPRINTF_MINOR).$(BPRINTF_PATCH)"
	mkdir -p $(shell dirname $@)
	$(BPRINTF_ROOT)/scripts/macro_gen.pl --nargs=$(BPRINTF_NARGS) --output=$(BPRINTF_HEADER)

bprintf_posthook:
	$(OBJDUMP) -s -j $(BPRINTF_SECTION) $(BPRINTF_TARGET.elf) > \
		$(BPRINTF_OUTPUT_DIR)/$(BPRINTF_SECTION).objdump
