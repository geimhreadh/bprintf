#! /usr/bin/env perl

use strict;
use warnings;
use Getopt::Long;

my %globopts;

sub string_db_gen {
    my ($idx, $buffer) = @_;
    my $hexstr = '';

    open(FH, '<', $globopts{input_file}) or
        die("*** Error: Unable to open $globopts{input} $!\n");

    while (<FH>) {
        # (<Address>) (<hex 1> <hex 2> <hex 3> <hex 4>) (<ASCII>)
        if (/^\s+([0-9a-f]{4,8})\s+(([0-9a-f]{2,8}\s+){1,4})(.*)$/) {
            $$idx = hex($1) if ($$idx == 0);
            $hexstr = $2 =~ s/\s+//gr;
            $$buffer .= pack('H*', $hexstr);
        }
    }
    close(FH);
}

sub string_find {
    my ($base_index, $string_buffer, $str_addr) = @_;

    # Find the format string, substr up to null character a la C string.
    my $offset = $str_addr - $base_index;
    my $str_offset = substr($string_buffer, $offset);
    my $substr = substr($str_offset, 0, index($str_offset, chr(0)));

    # Do some newline cleanup.
    chomp($substr); $substr .= "\n";

    return $substr;
}

sub print_string {
    my ($base_index, $string_buffer, @args) = @_;

    # Find the format string, substr up to null character a la C string.
    my $offset = $args[0] - $base_index;
    my $str_offset = substr($string_buffer, $offset);
    my $substr = substr($str_offset, 0, index($str_offset, chr(0)));

    # Do some newline cleanup.
    chomp($substr); $substr .= "\n";

    # Yes, I am aware of the danger...
    shift(@args);
    printf($substr, @args);
}

sub parse_cli {
    my ($input_file_ref, $isa_ref) = @_;

    GetOptions('input=s'  => \$globopts{input_file},
               'target=s' => \$globopts{target});
}

# A convenience function for figuring out the appropriate format for unpack().
# Not very elegant, but fit for the purpose of this example.
sub size2pack {
    my ($w) = @_;

    if ($w == 8) {
        return 'Q';
    } elsif ($w == 4) {
        return 'L';
    } elsif ($w == 2) {
        return 'S';
    } elsif ($w == 1) {
        return 'C';
    } else {
        return undef
    }
}

sub format2pack {
    my ($format, $size) = @_;

    my %formats = (
        'amd64' => {
            'signed_integer' => {
                8 => 'q', # int64_t
                4 => 'l', # int32_t
                2 => 's', # int16_t
                1 => 'c', # int8_t
            },
            'unsigned_integer' => {
                8 => 'Q', # uint64_t
                4 => 'L', # uint32_t
                2 => 'S', # uint16_t
                1 => 'C', # uint8_t
            },
            'floating_point' => {
                8 => 'd', # double
                4 => 'f', # float
            },
        },
    );

    if ($format =~ /[di]/) {
        return $formats{$globopts{target}}{signed_integer}{$size};
    } elsif ($format =~ /[uxX]/) {
        return $formats{$globopts{target}}{unsigned_integer}{$size};
    } elsif ($format =~ /[df]/) {
        return $formats{$globopts{target}}{floating_point}{$size};
    } else {
        return undef;
    }
}

sub formats_get {
    my ($format_str) = @_;
    my $remaining = $format_str;
    my $format_regexp = '%[0#]?[l]{0,2}([iduxXf])';
    my @formats = $format_str =~ /$format_regexp/g;

    return @formats;
}

sub run {
    my ($base_index, $str_buffer) = @_;
    my ($buffer, $size, $dbg_str, $nargs, $idx) =  ('', 0, 0, 0, 0);
    my @args;
    my @formats;
    my $arg;

    binmode(STDIN);

    # The contract with the target example is such that for each 'bprintf',
    # the target will send, in this order:
    #  - The size of the number of arguments.
    #  - The number of arguments (nargs)
    #  - The size of the debug string address.
    #  - The debug string address (dbg_str).
    # Followed by a series of size-data tuples of number (nargs). dbg_str and
    # the arguments are pushed into an array, and then the lookup and
    # subsequent formatting are performed thereon.
    while (read(STDIN, $buffer, 1) == 1) {
        $size = unpack('C', $buffer);
        read(STDIN, $buffer, $size);
        $nargs = unpack(size2pack($size), $buffer);

        @args = ();
        read(STDIN, $buffer, 1);
        $size = unpack('C', $buffer);
        read(STDIN, $buffer, $size);
        push(@args, unpack(size2pack($size), $buffer));

        # Look up string to get format specifiers, which will be mapped
        # to unpack() specifiers.
        @formats = formats_get(string_find($$base_index, $$str_buffer, $args[0]));

        for ($idx = 0; $idx < $nargs; $idx++) {
            read(STDIN, $buffer, 1);
            $size = unpack('C', $buffer);
            read(STDIN, $buffer, $size);
            $arg = unpack(format2pack($formats[$idx], $size), $buffer);
            push(@args, $arg);
        }
        print_string($$base_index, $$str_buffer, @args);
    }
}

sub dump_buffer {
    my ($buffer) = @_;
    print("BUFFER:  " . join(' ', map({ sprintf("%02X", ord($_)) } split(//, $buffer))) . "\n");
}

my $str_buffer = '';
my $base_index = 0;
parse_cli();
string_db_gen(\$base_index, \$str_buffer);
run(\$base_index, \$str_buffer);
