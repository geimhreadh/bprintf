#! /usr/bin/env perl

use strict;
use warnings;
use Getopt::Long;
use File::Basename;

my $NARGS = 2;
my $OUTPUT_FILE = "";

sub parse_cli {
    GetOptions('nargs=i' =>  \$NARGS,
               'output=s' => \$OUTPUT_FILE);

    if ($NARGS < 2 or $OUTPUT_FILE eq "") {
        die("Bad args.");
    }
}

sub gen_seq_macros {
    my ($fh, $nargs) = @_;
    my $i;
    # Generate VA_NARGS_N
    print($fh '#define VA_NARGS_N(');
    for ($i = 1; $i <= $nargs; $i++) {
        print($fh "_$i, ");
    }
    print($fh "N, ...) N\n");

    # Generate VA_NARGS_RSEQ
    print($fh '#define VA_NARGS_RSEQ ');
    for ($i = $nargs; $i > 0; $i--) {
        print($fh "$i, ");
    }
    print($fh "0\n");

    # Generate VA_NARGS_CSEQ
    print($fh '#define VA_NARGS_CSEQ ');
    for ($i = $nargs; $i > 1; $i--) {
        print($fh '1, ');
    }
    print($fh "0, 0\n");
}

sub gen_recursive_macros {
    my ($fh, $nargs) = @_;
    my $i;
    for ($i = 2; $i <= $nargs; $i++) {
        print($fh "#define MKDATUMS_$i(x, ...) MKDATUM(x), MKDATUMS_". ($i - 1) . "(__VA_ARGS__)\n");
    }
}

parse_cli();

my $fh;
open($fh, '>', $OUTPUT_FILE) or
    die("*** Error: Unable to open $OUTPUT_FILE: $!\n");

(my $include_guard = uc(basename($OUTPUT_FILE))) =~ s/\./_/g;
print($fh "#ifndef $include_guard\n#define $include_guard\n");
gen_seq_macros($fh, $NARGS);
gen_recursive_macros($fh, $NARGS);
print($fh "#endif\n");

close($fh);
