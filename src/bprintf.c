#include <stdarg.h>
#include <stdint.h>

#include "bprintf.h"

#ifdef UNUSED
#undef UNUSED
#endif
#define UNUSED(x) ((void)x)

void __attribute__((weak))
bprintf_write(uintptr_t str_addr, size_t nargs, ...) {
    UNUSED(str_addr);
    UNUSED(nargs);
}
